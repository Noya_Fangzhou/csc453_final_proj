load "revlog.rb"
def test_node
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.node(0) != "80b6e76643dc"
	puts "#{__method__} pass"
end
def test_rev
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.rev("80b6e76643dc") != 0
	puts "#{__method__} pass"
end
def test_parents
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.parents(0)[0] != "000000000000" or a.parents(0)[1] != "000000000000"
	puts "#{__method__} pass"
end
def test_start
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.start(1) != 5
	puts "#{__method__} pass"
end

def test_length
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.length(1) != 3
	puts "#{__method__} pass"
end

def test_fend
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.fend(1) != 7
	puts "#{__method__} pass"
end

def test_base
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.base(1) != 0
	puts "#{__method__} pass"
end

def test_revision
	a = Revlog.new("a.i","a.d")
	raise "#{__method__} error" if a.revision(0) != ["Fangzhou Liu\n", "Xindi Tang\n", "Tianxin Xie\n", "Yuxiao Chen\n"]
	puts "#{__method__} pass"
end

def test_addrevision_tip
	a = Revlog.new("a.i","a.d")
	test = "aa\nbb\n"
	a.addrevision(test)
	raise "#{__method__} error" if a.revision(a.tip) != ["aa\n", "bb\n"]
	puts "#{__method__} pass"
end

test_node
test_rev
test_parents
test_start
test_length
test_fend
test_base
test_revision
test_addrevision_tip
#puts "rev"
#puts a.rev('80b6e76643dc')
#puts "parents"
#puts a.parents(1)
#puts "start"
#puts a.start(1) 
#puts "length"
#puts a.length(1) 
#puts "end"
#puts a.fend(1) 
#puts "base"
#puts a.base(1) 



#puts a.index()