require './Repository.rb'
require './filelog_manifest_changelog.rb'
require './revlog.rb'
require 'fileutils'
require 'stringio'
require 'pathname'
# All Integration Test for Mercurial v0.1

# Go through all file in the directory find the file with the file_name given
# if match, return true, else return false
# @para file_name: file name to be searched within the directory
# @return result: true if founded, else false
def find_file?(file_name)
    cur_path = Dir.getwd # current working path
    # puts cur_path
    if not Dir.exist?(cur_path+"/.hg")
        return false
    end
    path = cur_path
    cur_dir = Dir.new(path)
    cur_dir.each do |fn|
        if fn == file_name
            return true
        end
    end
    return false
end

def with_captured_stdout
    begin
        old_stdout = $stdout
        $stdout = StringIO.new('','w')
        yield
        $stdout.string
        ensure
        $stdout = old_stdout
    end
end

# test the initialize method
def create_repo_test
    repo = Repository.new(".", 1)
    if find_file?("data") and find_file?("index")
        puts "The #{__method__} passed\n"
        else
        raise "ERROR! The #{__method__} failed, the repository doesn\'t created successfully\n"
    end
end

# test the add() method
def add_test
    repo = Repository.new(".", 0) # assume that the repository has already exist
    f = File.new('newfile', 'a+')
    repo.add('newfile')
    File.open('to-add','r') do |infile|
        while (line = infile.gets)
            if line == 'newfile'
                puts "The #{__method__} passed\n"
            end
        end
    end
    raise "ERROR! The #{__method__} failed, the file doesn\'t add to the repository"
end

# test the delete method
def delete_test
    repo = Repository.new('.', 0)
    repo.delete('newfile')
    if File.exist?('to-delete')
        puts "The #{__method__} passed\n"
        else
        raise "ERROR! The #{__method__} failed, the file doesn\'t delete from the repository"
    end
end

# test the commit method
def commit_test
    repo = Repository.new(".", 1)
    count = 0
    repo.commit
    File.open("./.hg/00changelog.d") do |infile|
        while(line = infile.gets)
            count += 1
        end
    end
    test_output(__method__, count == 1)
    
    
end

# test the checkout method
def checkout_test
    repo = Repository.new('.')
    repo.checkout(0)
    # File.open("current",'r') do |infile|
    #     if infile.empty?
    #         puts "The #{__method__} passed\n"
    #         else
    #         raise "ERROR! The #{__method__} failed, the checkout() method doesn\'t work"
    #     end
    # end
end

# print the test result
# @para method_name: the tested method name
# @para cond: condition to generate the test result
def test_output(method_name, cond)
    if cond
        puts "#{method_name} test passed\n"
        puts
        else
        raise "ERROR! The #{method_name} test failed\n"
        puts
    end
end

def test_case(name)
    puts "--------------------------\n"
    puts "#{name}:\n"
    puts "--------------------------\n"
end


# # test the dirdiff method
# def dirdiff_test
#   repo = repository.repository('.', 0)
#   repo.diffdir(repo.root)
# end

# create the repository and add 3 files
def create_add_test
    cond = true
    repo = Repository.new(".",1)
    # puts "Add a non-exist file in the repository"
    # name_ary = ["test_add0"]
    # repo.add(name_ary)
    # File.open("./.hg/to-add") do |infile|
    #   cond = (infile == "")
    # end
    # test_output(__method__, cond)
    
    # test_case("Add one file in the repository")
    # name_ary = ["test_add1_1"]
    # name_ary.each {|e| File.open(e, "a")}
    # repo.add(name_ary)
    # cond = File.exist?("./.hg/to-add")
    # test_output(__method__, cond)
    # puts
    
    # test_case("Add multiple file in the rpository")
    # name_ary = ["test_addn_1","test_addn_2","test_addn_3"]
    # name_ary.each {|e| File.open(e, "a")} # create these files
    # repo.add(name_ary)
    # cond = (File.exist?("./.hg/to-add") and (not File.size?("./.hg/to-add")))
    
    # File.open("./.hg/to-add") do |infile|
    #   line = infile.gets
    #   puts line
    #   cond = (line == "test_addn_1\\ntest_addn_2\\ntest_addn_3\\ntest_add1_1\\n")
    # end
    # test_output(__method__, cond)
    
end

FileUtils.rm_rf("./.hg")
create_add_test


# add -> diffdir
def add_diffdir_test
    repo = Repository.new(".")
    test_case("Delete one file and check the diff in the repository")
    name_ary = ["test_adddiff_0"]
    name_ary.each {|e| File.open(e, "a")}
    repo.add(name_ary)
    std_output = with_captured_stdout{repo.diffdir(".")}
    puts std_output
    cond = (std_output.split("\n").length == 13)
    test_output(__method__, cond)
    puts
    test_case("Delete multiple file and check the diff in the repository")
    name_ary = ["test_adddiff_1","test_adddiff_2","test_adddiff_3"]
    name_ary.each {|e| File.open(e, "a")}
    repo.add(name_ary)
    std_output = with_captured_stdout{repo.diffdir(".")}
    puts std_output
    cond = (std_output.split("\n").length == 19)
    test_output(__method__, cond)
end

# add_diffdir_test

def delete_diff_test
    repo = Repository.new(".")
    test_case("delete one file and check the diff in the repository")
    name_ary = ["test_adddiff_0"]
    name_ary.each {|e| File.open(e, "a")}
    repo.delete(name_ary)
    std_output = with_captured_stdout{repo.diffdir(".")}
    puts std_output
    cond = (std_output.split("\n").length == 20)
    test_output(__method__, cond)
    puts
    test_case("delete mutiple file and check the diff in the repository")
    name_ary = ["test_adddiff_1","test_adddiff_2","test_adddiff_3"]
    name_ary.each {|e| File.open(e, "a")}
    repo.delete(name_ary)
    std_output = with_captured_stdout{repo.diffdir(".")}
    puts std_output
    cond = (std_output.split("\n").length == 20)
    test_output(__method__, cond)
end

# delete_diff_test

# add file and then delete file
def add_delete_test
    cond = true
    repo = Repository.new(".")
    test_case("Add one file and delete this file in the repository")
    name_ary = ["test_adddo_1"]
    name_ary.each {|e| File.open(e, "a")}
    repo.add(name_ary)
    repo.delete(name_ary)
    cond = (File.exist?("./.hg/to-add") and File.exist?("./.hg/to-delete"))
    test_output(__method__, cond)
    puts
    test_case("Add multiple file and delete one file in the repository")
    name_ary = ["test_adddn_1", "test_adddn_2"]
    name_ary.each {|e| File.open(e,"a")}
    repo.add(name_ary)
    repo.delete(["test_adddn_2"])
    cond_1 = (File.exist?("./.hg/to-add") and File.exist?("./.hg/to-delete"))
    cond_2 = (not File.zero?("./.hg/to-add") and (not File.zero?("./.hg/to-delete")))
    cond = cond_1 and cond_2
    test_output(__method__, cond)
    puts
    # test("Delete non-exist file from the repository\n")
    # name_ary = ["test_addd0"]
    # repo.delete(name_ary)
    # File.open("./.hg/to-delete") do |infile|
    #  cond = (infile == nil)
    # end
    # test_output(__method__, cond)
end

# add_delete_test

# add file and then commit file
def add_commit_test
    cond = true
    test_case("Add one file and commit to the repository")
    repo = Repository.new(".")
    name_ary = ["test_aco_1"]
    name_ary.each {|e| File.open(e,"a")}
    repo.add(name_ary)
    repo.commit
    repo.checkout(0)
    # File.open("./.hg/00changelog.d") do |infile|
    #     # puts infile.read.split(",")[3]
    #     cond = (infile.gets.split(",")[3] == "test_aco_1")
    # end
    # test_output(__method__, cond)
    
    #  puts
    #  test_case("Add multiple file and commit to the repository")
    #  name_ary = ["test_acn_1","test_acn_2","test_acn_3"]
    #  name_ary.each {|e| File.open(e,"a")}
    #  repo.add(name_ary)
    #  repo.commit
    #  repo.checkout(repo.changelog.tip)
    #  File.open("./.hg/current") do |infile|
    #    cond = (infile.split(",")[3] == "['test_acn_1', 'test_acn_2', 'test_acn_3']")
    #  end
    #  test_output(__method__, cond)
    #  puts
    
    #  test_case("Add multiple file and commit multiple times to the repository")
    #  name_ary = ["test_acnc_1","test_acnc_2","test_acnc_3"]
    #  name_ary.each do |e|
    #    File.open(e, "a")
    #    repo.add([e])
    #    repo.commit
    #  end
    #  repo.checkout(repo.changelog.tip)
    #  File.open("./.hg/current") do |infile|
    #    cond = (infile.split(",")[3] == "['test_acnc_3']")
    #  end
    #  test_output(__method__, cond)
    #  puts
    
    #  test_case("commit a non-exist file to the repository")
    #  name_ary = ["test_acn0"]
    #  repo.commit
    #  repo.checkout(repo.changelog.tip)
    #  File.open("./.hg/current") do |infile|
    #    ccond = (infile.split(",")[3] == "['test_acnc_3']")
    #  end
    #  test_output(__method__, cond)
end

add_commit_test

def delete_commit_test
    repo = Repository.new(".")
    cond = true
    test_case("Delete one file and commit to the repository")
    name_ary = ["test_addn_1"]
    repo.delete(name_ary)
    repo.commit
    repo.checkout(repo.changelog.tip())
    File.open("./.hg/current") do |infile|
        cond = (infile.gets.split("\\n")[3] == "['test_addn_2','test_addn_3']")
    end
    test_output(__method__, cond)
    puts
    
    test_case("Delete multiple file and commit to the repository")
    name_ary = ["test_addn_2","test_addn_3"]
    repo.delete(name_ary)
    repo.commit
    repo.checkout(repo.changelog.tip())
    File.open("./.hg/current") do |infile|
        cond = (infile.gets.split("\\n")[3] == "[]")
    end
    test_output(__method__, cond)
    puts
    
end

# add -> commit -> delete -> commit
def add_commit_delete_commit_test
    repo = Repository.new(".")
    cond = true
    test_case("Add one file and then delete the same file after commit")
    name_ary = ["test_acdc1_1"]
    name_ary.each {|e| File.open(e, "a")}
    repo.add(name_ary)
    repo.commit
    repo.delete(name_ary)
    repo.commit
    repo.checkout(repo.changelog.tip())
    File.open("./.hg/current") do |infile|
        cond = (infile.gets.split("\\n")[3] == "[]")
    end
    test_output(__method__, cond)
    puts
end
