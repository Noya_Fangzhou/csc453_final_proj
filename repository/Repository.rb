require './revlog.rb'
require './filelog_manifest_changelog.rb'

class Repository
    attr_accessor :changelog
    attr_accessor :manifest
    def initialize(path=nil, create=false)
        if not path
            p = Dir.pwd
            while not File.directory?(File.join(p,'.hg'))
                p = File.dirname(p)
                raise 'No repo found' if p == '/'
            end
            path == p
        end
        @root = path
        path = File.join(@root, ".hg")
        @path = path
        
        if create
            Dir.mkdir(path)
            Dir.mkdir(File.join(path,"data"))
            Dir.mkdir(File.join(path,"index"))
        end
        self.manifest = Manifest.new(self)
        self.changelog = Changelog.new(self)
        
        begin
            @current = self.open("current").readline().to_i
            
            rescue
            @current = nil
        end
        
    end
    
    def open(p,mode="r")
        f = self.join(p)
        if mode == "a+" and File.file?(f)
            s = File.stat(f)
            if s.nlink > 1
                File(f+".tmp", "w+").write(File(f).read())
                File.rename(f+".tmp", f)
            end
        end
        return File.new(self.join(p),mode)
    end
    
    def join(f)
        return File.join(@path, f)
    end
    
    def file(f)
        return Filelog.new(self,f)
    end
    
    def merge(other)
        
    end
    
    def commit
        begin
            update = []
            f = self.open("to-add")
            f.each_line do |l|
                update << l[0..-2]
            end
            rescue Errno::ENOENT
            update = []
        end
        begin
            delete = []
            f = self.open("to-delete")
            f.each_line do |l|
                delete << l[0..-2]
            end
            rescue Errno::ENOENT
            delete = []
        end
        
        #check in files
        new = {}
        update.each do |f|
            r = Filelog.new(self,f)
            t = File.new(f).read()
            r.addrevision(t)
            new[f] = r.node(r.tip())
        end
        
        #update manifest
        old = @manifest.manifest(@manifest.tip())
        old.update(new)
        delete.each do |f|
            old[f] = nil
        end
        rev = @manifest.addmanifest(old)
        
        #add changeset
        new = new.keys
        new.sort ##
        n = @changelog.addchangeset(@manifest.node(rev), new, "commit")
        @current = n
        self.open("current", "w+").write(@current.to_s)
        
        toadd = self.join("to-add")
        FileUtils.rm_rf(toadd) if update
        todelete = self.join("to-delete")
        FileUtils.rm_rf(todelete) if delete
    end
    
    def checkdir(path)
        d = File.dirname(path)
        return if not d
        if not File.directory?(d)
            self.checkdir(d)
            Dir.mkdir(d)
        end
    end
    
    def checkout(rev)
        change = self.changelog.changeset(rev)
        mnode = change[0]
        puts "mnode: #{mnode}"
        puts "rev in checkout: #{self.manifest.rev(mnode)}"
        mmap = self.manifest.manifest(self.manifest.rev(mnode))
        st = self.open("dircache","w+")
        l = mmap.keys()
        l.sort()##
        l.each do |f|
            puts "f: #{f}"
            r = Filelog.new(self,f)
            t = r.revision(r.rev(mmap[f]))
            begin
                File(f,"w+").write(t)
                rescue
                self.checkdir(f)
                File(f,"w+").write(t)
            end
            
            s = File.stat(f)
            e = Tuple.new(s.mode, s.size, s.mtime, f.length)
            e = struct.pack(">llll", *e)##
            st.write(e+f)
        end
        
        @current = change
        self.open("current","w+").write(@current.to_s)
        
    end
    
    def diffdir(path)
        st = self.open("dircache", "r")
        dc = {}
        File.open(st).each do |line|
            fileInformationArray = line.split("    ||    ")
            beforee = []
            fileInformationArray.each do |x|
                beforee << x.split(" | ")
            end
            beforee.each do |e|
                l = e[3]
                f = e[4]
                dc[f] = e[0..3]
            end
        end
        
        changed = []
        added = []
        dir,subdirs,files = os_walk(@root)
        d = dir[(@root.size)-1 .. -1]
        subdirs -= [".hg"] if subdirs.include?(".hg")
        
        files.each do |f|
            fn = File.join(d, f)
            s = File.stat(fn)
            if dc.include?(fn)
                c = dc[fn]
                dc.delete(fn)
                if c[1] != s.size
                    changed << fn
                    p "C " + fn
                    elsif c[0] != s.mode or c[2] != s.mtime
                    t1 = File(fn).open() ## or read?
                    t2 = self.file(fn).revision(@current)
                    if t1 != t2
                        changed << fn
                        p "C "+fn
                    end
                end
                else
                added << fn
                p "A " + fn
            end
        end
        
        
        deleted = dc.keys
        deleted.sort
        deleted.each do |f|
            p "D " + f
        end
        
    end
    
    def os_walk(dir)
        root = Pathname(dir)
        files, dirs = [], []
        Pathname(root).find do |path|
            unless path == root
                dirs << path if path.directory?
                files << path if path.file?
            end
        end
        root_re = []
        files_re = []
        dirs_re = []
        
        files.each do |i|
            files_re << i.to_s
        end
        
        dirs.each do |i|
            dirs_re << i.to_s.split("/")[-1]
        end
        return [dir,dirs_re,files_re]
    end
    
    def add(list)
        al = self.open("to-add", "a+")
        st = self.open("dircache", "a+")
        list.each do |f|
            File.write(al,f+"\n")
            s = File.stat(f)
            e = [s.mode, s.size, s.mtime.to_i, f.length]
            File.write(st, s.mode.to_s+" | " + s.size.to_s+" | " + s.mtime.to_s+" | " + f.length.to_s+" | " + f +"    ||    ", File.size(st))
            
        end
    end
    
    def delete(list)
        dl = self.open("to-delete", "a+")
        list.each do |f|
            dl.write(f + "\n")
        end
    end
    
end
