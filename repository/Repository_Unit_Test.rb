require './Repository.rb'
require 'fileutils'
require 'pathname'
require "stringio"

def  test_init
  repo = Repository.new(".", 1)
  dir_exist = Dir.exists?("./.hg") && Dir.exists?("./.hg/data") && Dir.exists?("./.hg/index")
  removehg
  raise "#{__method__} error" if !dir_exist
  p "#{__method__} passed"
end

def  test_open
  repo = Repository.new(".", 1)
  repo.open('test.txt','a+')
  exist = File.exist?('./.hg/test.txt')
  removehg
  raise "#{__method__} error" if !exist
  p "#{__method__} passed"
end

def  test_join
  repo = Repository.new(".", 1)
  path = Dir.pwd
  expected = File.join('./.hg','test')
  actual = repo.join('test')
  removehg
  raise "#{__method__} error" if expected != actual
  p "#{__method__} passed"
end

def  test_file
  repo = Repository.new(".", 1)
  f = repo.file('demo')
  expected1 = f.instance_variable_get(:@path)
  expected2 = f.instance_variable_get(:@repository)
  removehg
  raise "#{__method__} error" if expected1 != 'demo' or expected2 != repo
  p "#{__method__} passed"
end

def test_commit##
  repo = Repository.new('.', 1)
  f1 = File.new('./Test/test1.txt', 'a+')
  f2 = File.new('./Test/test2.txt', 'a+')
  fn1 = './Test/test1.txt'
  fn2 = './Test/test2.txt'
  fileArray = []
  fileArray << fn1
  fileArray << fn2
  repo.add(fileArray)
  repo.commit
  removehg


  # new_count = count_entry("00changelog.d")
  # if (ori_count+1) == new_count
  #   puts "The #{__method__} passed\n"
  # else
  #   raise "ERROR! The #{__method__} failed, the file doesn\'t commit to the repository successfully"
  # end
end

def test_checkdir
  repo = Repository.new(".", 1)
  dir = repo.checkdir("./.hg/demo/test")
  actual = Dir.exist?("./.hg/demo")
  removehg
  raise "#{__method__} error" if  actual == false
  p "#{__method__} passed"
end


def test_checkout##
  repo = Repository.repository('.', 0)
  repo.checkout(1)
  File.open("current",'r') do |infile|
    if infile.empty?
      puts "The #{__method__} passed\n"
    else
      raise "ERROR! The #{__method__} failed, the checkout() method doesn\'t work"
    end
  end
end

def test_diffdir##
  repo = Repository.new('.', 1)
  Dir.mkdir("./Test")
  f1 = File.new('./Test/test1.txt', 'a+')
  f2 = File.new('./Test/test2.txt', 'a+')
  fn1 = './Test/test1.txt'
  fn2 = './Test/test2.txt'
  fileArray = []
  fileArray << fn1
  fileArray << fn2
  repo.add(fileArray)
  repo.commit
  str = with_captured_stdout{repo.diffdir(repo.instance_variable_get(:@root))}

  expected = "\"A ./.hg/current\"\n\"A ./.hg/dircache\"\n\"A ./.idea/misc.xml\"\n\"A ./.idea/modules.xml\"\n\"A ./.idea/untitled.iml\"\n\"A ./.idea/vcs.xml\"\n\"A ./.idea/workspace.xml\"\n\"C ./Test/test1.txt\"\n\"C ./Test/test2.txt\"\n\"A ./fake.rb\"\n\"A ./newfile\"\n\"A ./repository.rb\"\n\"A ./test.rb\"\n"

  raise "#{__method__} error" if str != expected
  p "#{__method__} passed"

  removehg
end

def with_captured_stdout
  begin
    old_stdout = $stdout
    $stdout = StringIO.new('','w')
    yield
    $stdout.string
  ensure
    $stdout = old_stdout
  end
end

def test_add
  repo = Repository.new('.', 1)
  Dir.mkdir("./Test")
  f1 = File.new('./Test/test1.txt', 'a+')
  f2 = File.new('./Test/test2.txt', 'a+')
  fn1 = './Test/test1.txt'
  fn2 = './Test/test2.txt'
  fileArray = []
  fileArray << fn1
  fileArray << fn2
  repo.add(fileArray)
  exist = File.exist?('./.hg/to-add') && File.exist?('./.hg/dircache')
  removehg
  raise "#{__method__} error" if exist == false
  p "#{__method__} passed"
end

def test_delete
  repo = Repository.new('.', 1)
  Dir.mkdir("./Test")
  f1 = File.new('./Test/test1.txt', 'a+')
  f2 = File.new('./Test/test2.txt', 'a+')
  fn1 = './Test/test1.txt'
  fn2 = './Test/test2.txt'
  fileArray = []
  fileArray << fn1
  fileArray << fn2
  repo.delete(fileArray)
  removehg
  raise "#{__method__} error" if File.exist?('./.hg/to-delete')
  p "#{__method__} passed"

end

def removehg
  FileUtils.rm_rf('./.hg')
  FileUtils.rm_rf('./Test')
end

removehg
test_init
test_open
test_join
test_file
#test_commit#
test_checkdir
#test_checkout
test_diffdir
test_add
test_delete


