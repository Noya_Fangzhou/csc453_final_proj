class Revlog
  def initialize(indexfile,datafile)
    @indexfile = indexfile
    @datafile = datafile
  end
  def tip
    return 5
  end
  def node(rev)
    return 001
  end
  def revision(rev)
    return "" if rev == -1
    return "aaabbb\nAAAbbb\n"
  end

  def addrevision(text,p1=nil,p2=nil)
    return text, 5
  end
end


class Filelog < Revlog
  #attr_reader :repository

  def initialize(repository,path)
    @repository = repository
    @path = path
    Revlog.new(path, path)
  end

  def open(file, mode = "r")
    return @repository.open(file, mode)
  end
end

class Manifest < Revlog
  #attr_reader :repository

  def initialize(repository)
    @repository = repository
    Revlog.new("00manifest.i","00manifest.d")
  end

  def open(file, mode="r")
    return repository.open(file,mode)
  end

  def manifest(rev)
    text = revision(rev)
    map = Hash.new
    for l in text.split("\n") do
      map[l[40...-1]] = l
    end
    return map
  end

  def addmanifest(map,p1=nil,p2=nil)

    return 6
  end
end


class Changelog < Revlog
  def initialize(repository)
    @repository = repository
    Revlog.new("00manifest.i","00manifest.d")
  end

  def open(file, mode="r")
    return @repository.open(file, mode)
  end

  def extract(text)
    l = text.split("\n")
    ll = Array.new
    for i in l do
      i.strip
      ll << i
    end
    manifest = ll[0]
    user = ll[1]
    data = ll[2]
    files = ll[3..-2]
    desc = ll[-1]
    l2 = Array.new
    l2[0] = manifest
    l2[1] = user
    l2[2] = data
    l2[3] = files
    l2[4] = desc
    return l2
  end

  def changeset(rev)
    return extract(revision(rev))
  end

  def addchangeset(manifest,list,desc,p1=nil,p2=nil)

  end
end
